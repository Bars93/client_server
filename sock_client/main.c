#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
// Posix-specific
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <syslog.h>
#include <errno.h>

const int def_port = 3418;

#define BUFFER_LEN 512
#define POSIX_CALL(val, call_func)\
	do {\
	errno = 0;\
	val = call_func;\
	if (val == -1)\
	{\
		syslog(LOG_ERR, "<%s:%d> Error with call system function \'%s\', ERRNO = %m", __func__, __LINE__,\
	       #call_func);\
		rc = 1;\
		goto exit;\
	}\
	else\
	{\
		syslog(LOG_INFO, "<%s> Successful call system function \'%s\'", __func__, #call_func);\
	}\
	} while(0)
#define NO_ARG_SYSLOG(num, fmt, ...) syslog(LOG_WARNING, "<%s> No passed %d arg. " fmt, __func__, num, ##__VA_ARGS__)
#define POSIX_CALL_ATEXIT(val, call_func)\
	do {\
		errno = 0;\
		val = call_func;\
		if (val == -1)\
		{\
			syslog(LOG_ERR, "<%s:%d> Error with call system function \'%s\', ERRNO = %m", __func__, __LINE__,\
			       #call_func);\
			rc = 1;\
		}\
		else\
		{\
			syslog(LOG_INFO, "<%s> Successful call system function \'%s\'", __func__, #call_func);\
		}\
	} while(0)
int main(int argc, char *argv[])
{
	int server_port = 0;
	int rc = 0, retval = 0;
	int oldmask = 0, msg_len = 0;
	int sockfd = -1;
	int is_stopped = 0;
	struct sockaddr_in server_addr;
	char buff[BUFFER_LEN];
	char server_ip[INET_ADDRSTRLEN];

	openlog("Irbis-sock_client", LOG_PID | LOG_CONS | LOG_NDELAY | LOG_PERROR, LOG_USER);
	oldmask = setlogmask(LOG_UPTO(LOG_DEBUG));
	if (argc > 1)
	{
		int arg_len = strlen(argv[1]);
		// minimal length of ipv4 address  n.n.n.n
		if (arg_len < 7)
		{
			syslog(LOG_ERR, "<%s> Error ip adrress is less than 7 characters", __func__);
			goto exit;
		}
	}
	else
	{
		NO_ARG_SYSLOG(1, "Error no passed server ip address. Exit");
		goto exit;
	}
	if (argc > 2)
	{
		server_port = atoi(argv[2]);
		if (0 == server_port)
		{
			syslog(LOG_WARNING, "<%s> Error converting 1st argument to port number. Port can't be 0\n", __func__);
			server_port = def_port;
		}
	}
	else
	{
		NO_ARG_SYSLOG(2, "Setting default port: %d", def_port);
		server_port = def_port;
	}
	fprintf(stdout, "Starting TCP (ipv4) client!\n\n");
	fprintf(stdout, "Configuration:\n");
	fprintf(stdout, "Server IP: %s\n", argv[1]);
	fprintf(stdout, "Port: %d\n", server_port);
	fflush(stdout);
	POSIX_CALL(sockfd, socket(AF_INET, SOCK_STREAM, IPPROTO_TCP));
	memset((void *) &server_addr, 0, sizeof(struct sockaddr_in));
	memset((void *) &buff, 0, sizeof(char) * BUFFER_LEN);
	memset((void *) server_ip, 0, sizeof(char) * INET_ADDRSTRLEN);
	server_addr.sin_family = AF_INET;
	server_addr.sin_addr.s_addr = inet_addr(argv[1]);
	server_addr.sin_port = htons(server_port);
	/* get server ip to string */
	strncpy(server_ip, inet_ntoa(server_addr.sin_addr), INET_ADDRSTRLEN);
	fprintf(stdout, "Connecting %s:%d\n", server_ip, server_port);
	fflush(stdout);
	POSIX_CALL(retval, connect(sockfd, (const struct sockaddr *) &server_addr, sizeof(struct sockaddr_in)));
	fprintf(stdout, "Connected!\n");
	/* Welcome message send */
	POSIX_CALL(msg_len, recv(sockfd, buff, BUFFER_LEN, 0));
	if (msg_len == 0) is_stopped = 1;
	else
	{
		fprintf(stdout, "%s\n[%s:%d]> ", buff, server_ip, server_port);
	}
	while (0 == is_stopped)
	{
		errno = 0;
		memset((void *) buff, 0, sizeof(char) * BUFFER_LEN);
		char *ret = fgets(buff, BUFFER_LEN, stdin);
		if (NULL == ret)
		{
			is_stopped = 1;
			syslog(LOG_ERR, "<%s> Error with read from stdin. ERRNO = %m", __func__);
			continue;
		}
		msg_len = strlen(buff);
		buff[msg_len - 1] = '\0';
//		while('\n' != *ret) ret--;
//		*ret = '\0'; msg_len--;
		POSIX_CALL(msg_len, send(sockfd, buff, msg_len, 0));
		if (msg_len > 0) syslog(LOG_INFO, "<%s> Message sended [%d bytes]", __func__, msg_len);
		memset((void *) buff, 0, sizeof(char) * BUFFER_LEN);
		POSIX_CALL(msg_len, recv(sockfd, buff, BUFFER_LEN, 0));
		if (msg_len > 0) syslog(LOG_INFO, "<%s> Message recieved [%d bytes]", __func__, msg_len);
		fprintf(stdout, "Message from server: %s\n[%s:%d]> ", buff, server_ip, server_port);
		if (0 == strcmp(buff, "quit")) is_stopped = 1;
	}
	fprintf(stdout, "Disconnected!\n");
exit:
	POSIX_CALL_ATEXIT(retval, close(sockfd));
	closelog();
	setlogmask(oldmask);

	return rc;
}
