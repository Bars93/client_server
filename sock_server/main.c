#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
// Posix-specific
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <syslog.h>
#include <errno.h>

const int def_port = 3418;
const int def_max_queue = 7;

#define BUFFER_LEN 512
#define POSIX_CALL(val, call_func)\
	do {\
	errno = 0;\
	val = call_func;\
	if (val == -1)\
	{\
		syslog(LOG_ERR, "<%s:%d> Error with call system function \'%s\', ERRNO = %m", __func__, __LINE__,\
		       #call_func);\
		rc = 1;\
		goto exit;\
	}\
	else\
	{\
		syslog(LOG_INFO, "<%s> Successful call system function \'%s\'", __func__, #call_func);\
	}\
	} while(0)
#define NO_ARG_SYSLOG(num, fmt, ...) syslog(LOG_WARNING, "<%s> No passed %d arg. " fmt, __func__, num, ##__VA_ARGS__)
#define POSIX_CALL_ATEXIT(val, call_func)\
	do {\
		errno = 0;\
		val = call_func;\
		if (val == -1)\
		{\
			syslog(LOG_ERR, "<%s:%d> Error with call system function \'%s\', ERRNO = %m", __func__, __LINE__,\
			       #call_func);\
			rc = 1;\
		}\
		else\
		{\
			syslog(LOG_INFO, "<%s> Successful call system function \'%s\'", __func__, #call_func);\
		}\
	} while(0)
int main(int argc, char *argv[])
{
	int listen_port = 0, max_queue = 0;
	int rc = 0, retval = 0;
	int oldmask = 0, cnt = 0, msg_len = 0;
	int sockfd = -1, cli_sock_fd = -1;
	int cli_sock_structlen = 0;
	int use_user_ip = 0, is_stopped = 0;
	struct sockaddr_in server_addr, client_addr;
	char buff[BUFFER_LEN];
	char client_ip[INET_ADDRSTRLEN];
	char server_ip[INET_ADDRSTRLEN];
	openlog("Irbis-sock_server", LOG_PID | LOG_CONS | LOG_NDELAY | LOG_PERROR, LOG_USER);
	oldmask = setlogmask(LOG_UPTO(LOG_DEBUG));
	if (argc > 1)
	{
		int arg_len = strlen(argv[1]);
		// minimal length of ipv4 address  n.n.n.n
		if (arg_len >= 7) use_user_ip = 1;
	}
	else
	{
		NO_ARG_SYSLOG(1, "Select default ip address");
	}
	if (argc > 2)
	{
		listen_port = atoi(argv[2]);
		if (0 == listen_port)
		{
			syslog(LOG_WARNING, "<%s> Error converting 1st argument to port number. Port can't be 0\n", __func__);
			listen_port = def_port;
		}
	}
	else
	{
		NO_ARG_SYSLOG(2, "Setting default port: %d", def_port);
		listen_port = def_port;
	}
	if (argc > 3)
	{
		max_queue = atoi(argv[3]);
		if (0 == max_queue)
		{
			syslog(LOG_WARNING, "<%s> Error converting %d argument to queue limitation. It\'s can not be 0", 
			       __func__, 3);
			max_queue = def_max_queue;
		}
	}
	else
	{
		NO_ARG_SYSLOG(3, "Setting default max queue limitation to %d", def_max_queue);
		max_queue = def_max_queue;
	}
	fprintf(stdout, "Starting TCP (ipv4) server!\n\n");
	fprintf(stdout, "Configuration:\n");
	fprintf(stdout, "IP: %s\n", (1 == use_user_ip) ? argv[1] : "0.0.0.0");
	fprintf(stdout, "Port: %d\n", listen_port);
	fprintf(stdout, "Max queue limitation: %d\n\n", max_queue);
	fflush(stdout);
	POSIX_CALL(sockfd, socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)); 
	memset((void *) &server_addr, 0, sizeof(struct sockaddr_in));
	memset((void *) &buff, 0, sizeof(char) * BUFFER_LEN);
	memset((void *) client_ip, 0, sizeof(char) * INET_ADDRSTRLEN);
	memset((void *) server_ip, 0, sizeof(char) * INET_ADDRSTRLEN);
	server_addr.sin_family = AF_INET;
	server_addr.sin_addr.s_addr = (1 == use_user_ip) ? inet_addr(argv[1]) : INADDR_ANY;
	server_addr.sin_port = htons(listen_port);
	/* get server ip to string */
	strncpy(server_ip, (const char *) inet_ntoa(server_addr.sin_addr), INET_ADDRSTRLEN);
	POSIX_CALL(retval, bind(sockfd, (const struct sockaddr *) &server_addr, sizeof(struct sockaddr_in)));
	fprintf(stdout, "Listening %s:%d\n", server_ip, listen_port);
	fflush(stdout);
	POSIX_CALL(retval, listen(sockfd, max_queue));
	cli_sock_structlen = sizeof(struct sockaddr_in);
	POSIX_CALL(cli_sock_fd, accept(sockfd, (struct sockaddr *) &client_addr, &cli_sock_structlen));
	/* get client ip */
	strncpy(client_ip, inet_ntoa(client_addr.sin_addr), INET_ADDRSTRLEN);
	/* echo server */
	fprintf(stdout, "Connected client: %s:%d\n", client_ip, ntohs(client_addr.sin_port));
	/* Welcome message send */
	cnt = snprintf(buff, sizeof(char) * BUFFER_LEN,
	        "\n%s is welcome you\n"\
	        "Server address: %s:%d\n"\
	        "Client address: %s:%d\n"\
	        "Enter \'quit\' for disconnect and exit\n\n",
	        "Irbis-sock_server",
	        server_ip, listen_port,
	        client_ip, ntohs(client_addr.sin_port)
	        );
	if (cnt < 0)
	{
		is_stopped = 1;
		syslog(LOG_ERR, "<%s> Error print message to buffer. Buffer size: %d. Returned by snprintf: %d", __func__,
		       BUFFER_LEN, cnt);
	}
	else
	{
		msg_len = strlen(buff);
		POSIX_CALL(retval, write(cli_sock_fd, buff, msg_len));
		syslog(LOG_INFO, "<%s> Message sended [%d bytes]", __func__, msg_len);
	}
	while (0 == is_stopped)
	{
		// descriptor validation
		errno = 0;
		memset((void *) buff, 0, sizeof(char) * BUFFER_LEN);
		POSIX_CALL(msg_len, recv(cli_sock_fd, buff, BUFFER_LEN, 0));
		fprintf(stdout, "Message: %s\n", buff);
		fflush(stdout);
		if (!strcmp(buff, "quit"))
		{
			POSIX_CALL(retval, send(cli_sock_fd, "quit", 5, 0));
			is_stopped = 1;
			continue;
		}
		if (msg_len > 0) syslog(LOG_INFO, "<%s> Message recieved [%d bytes]", __func__, msg_len);
		POSIX_CALL(msg_len, send(cli_sock_fd, buff, msg_len, 0));
		if (msg_len > 0) syslog(LOG_INFO, "<%s> Message sended [%d bytes]", __func__, msg_len);
	}
	fprintf(stdout, "Stop TCP server!\n");
exit:
	POSIX_CALL_ATEXIT(retval, close(sockfd));
	POSIX_CALL_ATEXIT(retval, close(cli_sock_fd));
	closelog();
	setlogmask(oldmask);

	return rc;
}
